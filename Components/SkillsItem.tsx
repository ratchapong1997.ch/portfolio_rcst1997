import React from 'react'

interface Props {
    title : string;
    year: string;
}
const SkillsItem = ({title,year}:Props) => {
  return (
    <div data-aos="fade-up" data-aos-delay="300" className="mb-[4rem] md:mb-[8rem]">
        <span className="px-[2rem] text-[#55e6a5] py-[0.9rem] font-bold text-[18px] border-[2px] border-[#55e6a5]">
            {year}
        </span>
        <h1 className="mt-[2rem] uppercase font-semibold mb-[1rem] text-[20px] sm:text-[25px] text-white">
            {title}
        </h1>
        <p className="text-[#aaaaaa] font-normal w-[80%] text-[17px] opacity-80">
            Lorem ipsum dolor sit amet consectetur adipisicing elit.
            Ipsa culpa accusantium adipisci temporibus eos quod ex
            fuga facere vero molestiae consectetur veritatis vel,
            aliquid iure?
        </p>
    </div>
  )
}

export default SkillsItem